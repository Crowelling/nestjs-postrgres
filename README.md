<p align="center">
  <img src="https://nestjs.com/img/logo_text.svg" width="300" alt="Nest Logo" />
 </p>
<p align="center">
  <img src="https://cdn.iconscout.com/icon/free/png-512/typescript-1174965.png" width="100" alt="TS Logo" />
  <img src="https://www.k-lagan.com/wp-content/uploads/2016/11/AWS.png" width="170" alt="AWS Logo" />
  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Postgresql_elephant.svg/745px-Postgresql_elephant.svg.png" width="100" alt="PostgreSQL Logo" />
</p>
 
## Task Manager Application
• CRUD operations

• Validation and Errors handling

• Data Persistence - PostgreSQL & TypeORM

• Authentication and Authorization - JWT/Passport.js

• Logging and Testing

• Deployment - Amazon Web Services

## Start the app locally

```bash
#before start
$ npm install

# start postgreSQL and pgAdmin4
 - plese use documentation to start, run and create local db

# start project
$ npm run start
```
